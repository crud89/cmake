cmake_minimum_required(VERSION 3.29)
include(RunCMake)

run_cmake(Default)
run_cmake(Explicit)
